One Thud to Dres
================

.. video:: QJb3uWuiuT4
   :frame: 2:00


Thumbnail
---------

"Trapped". Rigel complaining about his fate.

2:00
----

The link in the clear is a joke.

Direct :doc:`/stratish` translation:
    *Walls of the etarnal prison is a singular calling Akogu Tinia Krot*os says not today*

This was resolved to mean:
    *The walls of the eternal prison are a singular calling" says Kronos. "Not Today" says Zeus.*

Kronos and Zeus are specialized viruses which steal online banking information,
so this could be an early connection to the main storyline. 

Apparently this translation was wrong.
We knew it when we saw qj8U-k1z4eA and found exactly the same pic near the end.
The right translation was:

    *Walls of is the prison eternal called singular. Kagou Anti KroSoft says not today.*
